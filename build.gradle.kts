import com.google.protobuf.gradle.generateProtoTasks
import com.google.protobuf.gradle.id
import com.google.protobuf.gradle.plugins
import com.google.protobuf.gradle.protobuf
import com.google.protobuf.gradle.protoc

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val prometeus_version: String by project
val janino_version: String by project
val loki4j_version: String by project
val opentelemetry_version: String by project
val grpc_version: String by project
val grpc_kotlin_version: String by project
val protobuf_version: String by project
val jansi_version: String by project

plugins {
  kotlin("jvm") version "1.6.10"
  id("idea")
  id("java-library")
  id("maven-publish")
  id("org.jlleitschuh.gradle.ktlint") version "10.2.1"
  id("com.google.protobuf") version "0.8.18"
}

group = "com.andreeff"
version = "0.3.0"

repositories {
  mavenCentral()
  maven { url = uri("https://maven.pkg.jetbrains.space/public/p/ktor/eap") }
}

dependencies {
  implementation("io.ktor:ktor-server-core:$ktor_version")
  implementation("io.ktor:ktor-server-locations:$ktor_version")
  implementation("io.ktor:ktor-server-host-common:$ktor_version")
  implementation("io.ktor:ktor-server-content-negotiation:$ktor_version")
  implementation("io.ktor:ktor-server-http-redirect:$ktor_version")
  implementation("io.ktor:ktor-server-call-logging:$ktor_version")
  implementation("io.ktor:ktor-server-metrics:$ktor_version")
  implementation("io.ktor:ktor-server-metrics-micrometer:$ktor_version")
  implementation("io.micrometer:micrometer-registry-prometheus:$prometeus_version")
  implementation("io.ktor:ktor-server-netty:$ktor_version")
  implementation("org.codehaus.janino:janino:$janino_version")
  implementation("com.github.loki4j:loki-logback-appender:$loki4j_version")
  implementation("ch.qos.logback:logback-classic:$logback_version")
  implementation("io.ktor:ktor-serialization-kotlinx-json:$ktor_version")
  implementation("io.opentelemetry:opentelemetry-api:$opentelemetry_version")
  implementation("io.opentelemetry:opentelemetry-sdk:$opentelemetry_version")
  implementation("io.opentelemetry:opentelemetry-exporter-otlp:$opentelemetry_version")
  implementation("io.opentelemetry:opentelemetry-extension-kotlin:$opentelemetry_version")
  implementation("io.ktor:ktor-client-core:$ktor_version")
  implementation("io.ktor:ktor-client-cio:$ktor_version")
  implementation("io.grpc:grpc-netty:$grpc_version")
  implementation("io.grpc:grpc-protobuf:$grpc_version")
  implementation("io.grpc:grpc-stub:$grpc_version")
  implementation("io.grpc:grpc-kotlin-stub:$grpc_kotlin_version")
  implementation("com.google.protobuf:protobuf-kotlin:$protobuf_version")
  implementation("org.fusesource.jansi:jansi:$jansi_version")
  testImplementation("io.ktor:ktor-server-tests:$ktor_version")
  testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
  testImplementation("io.opentelemetry:opentelemetry-sdk-testing:$opentelemetry_version")
}

protobuf {
  protoc {
    artifact = "com.google.protobuf:protoc:$protobuf_version"
  }
  plugins {
    id("grpc") {
      artifact = "io.grpc:protoc-gen-grpc-java:$grpc_version"
    }
    id("grpckt") {
      artifact = "io.grpc:protoc-gen-grpc-kotlin:$grpc_kotlin_version:jdk7@jar"
    }
    id("grpc-gateway") {
      artifact = "io.grpc:protoc-gen-grpc-java:$grpc_version"
    }
  }
  generateProtoTasks {
    all().forEach {
      it.plugins {
        id("grpc")
        id("grpckt")
      }
      it.builtins {
        id("kotlin")
      }
    }
  }
  generatedFilesBaseDir = "$projectDir/gen"
}

publishing {
  publications {
    create<MavenPublication>("mavenJava") {
      from(components["java"])
      afterEvaluate {
        artifactId = tasks.jar.get().archiveBaseName.get()
      }
      versionMapping {
        usage("java-api") {
          fromResolutionOf("runtimeClasspath")
        }
        usage("java-runtime") {
          fromResolutionResult()
        }
      }
    }
  }
  repositories {
    maven {
      url = uri("${System.getenv("CI_API_V4_URL")}/projects/33035189/packages/maven")
      credentials(HttpHeaderCredentials::class) {
        name = "Job-Token"
        value = System.getenv("CI_JOB_TOKEN")
      }
      authentication {
        create<HttpHeaderAuthentication>("header")
      }
    }
  }
}
