# CringeX (Kotlin)

Ktor-based Kotlin microservice core

## Dependencies
* Ktor
* Micrometrics
* Logback
* OpenTelemetry

## Functionality
1. Health checking
2. Logging to Loki
3. Collecting metrics for Prometheus
4. Tracing to Tempo via OTLP

## Authors
Alexander Andreev a.k.a Andreeff a.k.a AlexanderFromEarth a.k.a NaheRidi
