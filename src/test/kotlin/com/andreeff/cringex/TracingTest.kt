package com.andreeff.cringex

import com.andreeff.cringex.client.plugins.Tracing
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.testing.*
import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.common.AttributeKey.*
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator
import io.opentelemetry.context.propagation.ContextPropagators
import io.opentelemetry.sdk.OpenTelemetrySdk
import io.opentelemetry.sdk.testing.exporter.InMemorySpanExporter
import io.opentelemetry.sdk.trace.SdkTracerProvider
import io.opentelemetry.sdk.trace.export.SimpleSpanProcessor
import org.junit.Test
import kotlin.test.*

class TracingTest {
  @Test
  fun testSuccessCheck() = testApplication {
    val exporter = InMemorySpanExporter.create()

    GlobalOpenTelemetry.resetForTest()
    OpenTelemetrySdk.builder()
      .setTracerProvider(
        SdkTracerProvider
          .builder()
          .addSpanProcessor(SimpleSpanProcessor.create(exporter))
          .build()
      )
      .setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
      .buildAndRegisterGlobal()

    val client = createClient {
      install(Tracing) {
        tag("test") { "test" }
      }
    }

    application {
      install(ContentNegotiation) {
        json()
      }
      install(com.andreeff.cringex.server.plugins.Tracing)
      routing {
        get("/") {
          val response = client.get("/test") {
            expectSuccess = false
          }

          call.respond(response.status, response.bodyAsText())
        }
        get("/test") {
          call.respond(HttpStatusCode.BadRequest, "test")
        }
      }
    }
    client.get("/") {
      expectSuccess = false
    }

    assertEquals("test", exporter.finishedSpanItems.last().attributes[stringKey("test")])
    assertEquals(true, exporter.finishedSpanItems.last().attributes[booleanKey("error")])
  }
}
