package com.andreeff.cringex

import com.andreeff.cringex.server.plugins.Health
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.*
import io.ktor.server.testing.*
import org.junit.Test
import kotlin.test.*

class HealthTest {
  @Test
  fun testSuccessCheck() = testApplication {
    application {
      install(ContentNegotiation) {
        json()
      }
      install(Health) {
        check("application") { true }
      }
    }

    val response = this.client.get("/healthz")

    assertEquals(HttpStatusCode.OK, response.status)
  }

  @Test
  fun testFailureCheck() = testApplication {
    application {
      install(ContentNegotiation) {
        json()
      }
      install(Health) {
        check("application") { false }
      }
    }

    val response = this.client.get("/healthz") {
      expectSuccess = false
    }

    assertEquals(HttpStatusCode.ServiceUnavailable, response.status)
  }
}
