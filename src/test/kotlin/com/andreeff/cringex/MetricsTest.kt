package com.andreeff.cringex

import com.andreeff.cringex.server.plugins.Metrics
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.*
import io.ktor.server.testing.*
import io.micrometer.core.instrument.binder.jvm.*
import io.micrometer.core.instrument.binder.system.*
import io.micrometer.prometheus.*
import org.junit.Test
import kotlin.test.*

class MetricsTest {
  @Test
  fun testSuccessCheck() = testApplication {
    application {
      val prometheus = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

      install(ContentNegotiation) {
        json()
      }
      install(Metrics) {
        metrics = {
          registry = prometheus
          meterBinders = listOf(
            JvmMemoryMetrics(),
            JvmGcMetrics(),
            ProcessorMetrics(),
            UptimeMetrics(),
            JvmInfoMetrics()
          )
        }
      }
    }

    val response = this.client.get("/metrics")

    assertEquals(response.status, HttpStatusCode.OK)
    assertNotEquals(response.bodyAsText(), "")
  }
}
