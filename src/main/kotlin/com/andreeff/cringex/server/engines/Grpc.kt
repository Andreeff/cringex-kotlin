package com.andreeff.cringex.server.engines

import io.ktor.server.engine.*

object Grpc : ApplicationEngineFactory<GrpcApplicationEngine, GrpcApplicationEngine.Configuration> {
  override fun create(
    environment: ApplicationEngineEnvironment,
    configure: GrpcApplicationEngine.Configuration.() -> Unit
  ): GrpcApplicationEngine =
    GrpcApplicationEngine(environment, configure)
}
