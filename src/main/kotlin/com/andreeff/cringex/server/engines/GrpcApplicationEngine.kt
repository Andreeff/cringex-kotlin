package com.andreeff.cringex.server.engines

import io.grpc.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import java.util.concurrent.TimeUnit

class GrpcApplicationEngine(
  environment: ApplicationEngineEnvironment,
  configure: Configuration.() -> Unit =
    {}
) : BaseApplicationEngine(environment) {
  class Configuration : BaseApplicationEngine.Configuration() {
    var grpc: ServerBuilder<*>.() -> Unit = {}
  }

  private var server: Server = ServerBuilder
    .forPort(environment.connectors.first().port)
    .apply(Configuration().apply(configure).grpc)
    .build()

  override fun start(wait: Boolean): ApplicationEngine {
    server.start()

    if (wait) {
      server.awaitTermination()
    }

    return this
  }

  override fun stop(gracePeriodMillis: Long, timeoutMillis: Long) {
    environment.monitor.raise(ApplicationStopPreparing, environment)
    server.awaitTermination(gracePeriodMillis, TimeUnit.MILLISECONDS)
    server.shutdownNow()
  }
}
