package com.andreeff.cringex.server.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.metrics.micrometer.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.*
import io.micrometer.prometheus.PrometheusMeterRegistry

class Metrics(private val config: Configuration) {
  class Configuration {
    var metrics: MicrometerMetrics.Configuration.() -> Unit = {}
    var path: String = "/metrics"
  }

  companion object Plugin : ApplicationPlugin<Application, Configuration, Metrics> {
    override val key: AttributeKey<Metrics> = AttributeKey("Metrics")

    override fun install(pipeline: Application, configure: Configuration.() -> Unit): Metrics {
      val metrics = Metrics(Configuration().apply(configure))

      MicrometerMetrics.install(pipeline, metrics.config.metrics)

      pipeline.routing {
        get(metrics.config.path) {
          val micrometerMetricsConfig = MicrometerMetrics.Configuration().apply(metrics.config.metrics)

          if (micrometerMetricsConfig.registry !is PrometheusMeterRegistry) {
            this.call.respond(HttpStatusCode.NotImplemented, "")
          } else {
            this.call.respond((micrometerMetricsConfig.registry as PrometheusMeterRegistry).scrape())
          }
        }
      }

      return metrics
    }
  }
}
