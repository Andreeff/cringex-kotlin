package com.andreeff.cringex.server.plugins

import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import io.ktor.util.*
import io.ktor.util.pipeline.*
import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.trace.Span
import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.Context
import io.opentelemetry.context.propagation.TextMapGetter
import io.opentelemetry.extension.kotlin.asContextElement
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import org.slf4j.MDC
import io.opentelemetry.api.common.AttributeKey as OpenTelemetryAttributeKey

class Tracing(private val config: Configuration) {
  class Configuration {
    var instrumentation: String = "CringeX"
    val filters: MutableList<(ApplicationCall) -> Boolean> = mutableListOf()
    val tags: MutableList<Pair<String, () -> String>> = mutableListOf()

    fun filter(predicate: (ApplicationCall) -> Boolean) {
      filters += predicate
    }

    fun tag(name: String, lambda: () -> String) {
      tags += name to lambda
    }
  }

  private val tracer: Tracer = GlobalOpenTelemetry.getTracer(this.config.instrumentation)

  companion object Plugin : ApplicationPlugin<Application, Configuration, Tracing> {
    override val key: AttributeKey<Tracing> = AttributeKey("Tracing")

    override fun install(pipeline: Application, configure: Configuration.() -> Unit): Tracing {
      val tracing = Tracing(Configuration().apply(configure))
      val startTracingPhase = PipelinePhase("TracingStart")
      val finishTracingPhase = PipelinePhase("TracingFinish")

      pipeline.insertPhaseAfter(ApplicationCallPipeline.Setup, startTracingPhase)
      pipeline.insertPhaseAfter(ApplicationCallPipeline.Fallback, finishTracingPhase)
      pipeline.intercept(startTracingPhase) {
        if (tracing.config.filters.isNotEmpty() && tracing.config.filters.all { !it(call) }) return@intercept

        val parent = GlobalOpenTelemetry
          .getPropagators()
          .textMapPropagator
          .extract(
            Context.current(),
            context.request,
            object : TextMapGetter<ApplicationRequest> {
              override fun keys(carrier: ApplicationRequest): MutableIterable<String> =
                carrier.headers.toMap().keys.toMutableSet()

              override fun get(carrier: ApplicationRequest?, key: String): String? =
                carrier?.headers?.get(key)
            }
          )

        withContext(parent.asContextElement()) {
          val span = tracing.tracer
            .spanBuilder("${context.request.httpMethod.value} ${context.request.path()}")
            .setSpanKind(SpanKind.SERVER)
            .setParent(parent)
            .setAttribute(
              OpenTelemetryAttributeKey.stringKey("http.method"),
              context.request.httpMethod.value
            )
            .setAttribute(OpenTelemetryAttributeKey.stringKey("http.target"), context.request.path())
            .setAttribute(OpenTelemetryAttributeKey.stringKey("http.host"), context.request.host())
            .startSpan()

          MDC.put("traceId", span.spanContext.traceId)
          tracing.config.tags.forEach {
            span.setAttribute(it.first, it.second.invoke())
          }
          coroutineContext[Job]?.invokeOnCompletion {
            it?.also {
              span.setAttribute(OpenTelemetryAttributeKey.booleanKey("error"), true)
              span.recordException(it)
              finish()
            }
          }

          withContext(span.asContextElement()) {
            proceed()
          }
        }
      }
      pipeline.environment.monitor.subscribe(Routing.RoutingCallStarted) { call ->
        if (tracing.config.filters.all { !it(call) }) return@subscribe

        val span = Span.current() ?: return@subscribe

        span.updateName(
          "${call.request.httpMethod.value} ${
          call.parameters.entries().fold(call.request.path()) { path, param ->
            span.setAttribute(param.key, param.value.first())
            path.replaceFirst(param.value.first(), "{${param.key}}")
          }
          }"
        )
      }
      pipeline.intercept(finishTracingPhase) {
        if (tracing.config.filters.all { !it(call) }) return@intercept

        val span = Span.current() ?: return@intercept
        val statusCode = context.response.status()

        if ((statusCode?.value ?: 500) >= 400) {
          span.setAttribute(OpenTelemetryAttributeKey.booleanKey("error"), true)
          span.setStatus(StatusCode.ERROR)
        } else {
          span.setStatus(StatusCode.OK)
        }

        span.setAttribute(
          OpenTelemetryAttributeKey.longKey("http.status_code"),
          statusCode?.value ?: 500
        )
        span.end()
      }

      return tracing
    }
  }
}
