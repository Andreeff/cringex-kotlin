package com.andreeff.cringex.server.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.*

class Health(private val config: Configuration) {
  class Configuration {
    val checks: MutableMap<String, suspend () -> Boolean> = mutableMapOf()
    var path: String = "/healthz"

    fun check(name: String, check: suspend () -> Boolean) {
      this.checks += name to check
    }
  }

  companion object Plugin : ApplicationPlugin<Application, Configuration, Health> {
    override val key: AttributeKey<Health> = AttributeKey("Health")

    override fun install(pipeline: Application, configure: Configuration.() -> Unit): Health {
      val health = Health(Configuration().apply(configure))

      pipeline.routing {
        get(health.config.path) {
          val checks = health.config.checks.mapValues { it.value() }

          this.call.respond(
            if (checks.values.all { it }) HttpStatusCode.OK else HttpStatusCode.ServiceUnavailable,
            checks
          )
        }
      }

      return health
    }
  }
}
