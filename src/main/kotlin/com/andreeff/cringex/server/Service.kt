package com.andreeff.cringex.server

import com.andreeff.cringex.server.engines.Grpc
import com.andreeff.cringex.server.interceptors.Logging
import com.andreeff.cringex.server.plugins.Health
import com.andreeff.cringex.server.plugins.Metrics
import io.grpc.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import io.micrometer.core.instrument.*
import io.micrometer.core.instrument.binder.jvm.*
import io.micrometer.core.instrument.binder.system.*
import io.micrometer.prometheus.*
import io.opentelemetry.api.*
import io.opentelemetry.api.common.AttributeKey
import io.opentelemetry.api.common.Attributes
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator
import io.opentelemetry.context.propagation.ContextPropagators
import io.opentelemetry.exporter.otlp.trace.OtlpGrpcSpanExporter
import io.opentelemetry.sdk.OpenTelemetrySdk
import io.opentelemetry.sdk.resources.Resource
import io.opentelemetry.sdk.trace.SdkTracerProvider
import io.opentelemetry.sdk.trace.export.BatchSpanProcessor
import com.andreeff.cringex.client.interceptors.Tracing as GrpcClientTracing
import com.andreeff.cringex.client.plugins.Tracing as HttpClientTracing
import com.andreeff.cringex.server.interceptors.Tracing as GrpcServerTracing
import com.andreeff.cringex.server.plugins.Tracing as HttpServerTracing

class Service(configure: Configuration.() -> Unit) {
  class Configuration {
    var serviceName: String = System.getenv("SERVICE_NAME") ?: "CringeX"
    var isBusiness: Boolean = false
    var httpPort: Int = 8080
    var grpcPort: Int = 6565
    var http: Application.() -> Unit = {}
    var grpc: ServerBuilder<*>.() -> Unit = {}
    var openTelemetry: OpenTelemetry =
      OpenTelemetrySdk.builder()
        .setTracerProvider(
          SdkTracerProvider.builder()
            .setResource(
              Resource.create(
                Attributes
                  .builder()
                  .put(AttributeKey.stringKey("service"), serviceName)
                  .put(AttributeKey.stringKey("service.name"), serviceName)
                  .build()
              )
            )
            .addSpanProcessor(
              BatchSpanProcessor
                .builder(
                  OtlpGrpcSpanExporter
                    .builder()
                    .setEndpoint(System.getenv("OTLP_URL"))
                    .build()
                )
                .build()
            )
            .build()
        )
        .setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
        .build()
    var meterRegistry: MeterRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

    fun httpClient(): HttpClient =
      HttpClient(CIO) {
        install(HttpClientTracing)
      }

    fun grpcChannel(name: String, port: Int): ManagedChannel =
      ManagedChannelBuilder
        .forAddress(name, port)
        .intercept(GrpcClientTracing)
        .usePlaintext()
        .build()
  }

  private val config: Configuration = Configuration().apply(configure)

  fun start() {
    GlobalOpenTelemetry.set(config.openTelemetry)
    embeddedServer(
      Grpc,
      port = config.grpcPort,
      host = "localhost",
      configure = {
        grpc = {
          intercept(GrpcServerTracing)
          intercept(Logging)
          apply(config.grpc)
        }
      }
    ) {}.start(false)
    embeddedServer(
      Netty,
      port = config.httpPort,
      host = "localhost",
    ) {
      install(CallLogging) {
        filter { call -> call.request.path().startsWith("/") && !call.request.path().startsWith("/metrics") }
        format { call ->
          val status = call.response.status()
          val method = call.request.httpMethod.value
          val route = call.request.path()
          val headers = call.request.headers.entries().joinToString(", ", " {", "} ") { header ->
            "${header.key}: ${header.value[0]}"
          }
          "$status: $method - $route$headers"
        }
      }
      install(Metrics) {
        metrics = {
          metricName = config.serviceName
          registry = config.meterRegistry
          meterBinders = listOf(
            JvmMemoryMetrics(),
            JvmGcMetrics(),
            ProcessorMetrics(),
            UptimeMetrics(),
            JvmInfoMetrics()
          )
        }
      }
      install(ContentNegotiation) { json() }
      install(HttpServerTracing) {
        filter { call -> call.request.path().startsWith("/") && !call.request.path().startsWith("/metrics") }
      }
      install(Health)
      apply(config.http)
    }.start(true)
  }
}
