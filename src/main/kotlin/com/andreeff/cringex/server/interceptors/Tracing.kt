package com.andreeff.cringex.server.interceptors

import io.grpc.*
import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.common.AttributeKey
import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.Context
import io.opentelemetry.context.propagation.TextMapGetter
import org.slf4j.MDC

open class Tracing(configure: Configuration.() -> Unit = {}) : ServerInterceptor {
  class Configuration {
    var instrumentation: String = "CringeX"
    val filters: MutableList<(ServerCall<*, *>) -> Boolean> = mutableListOf()
    val tags: MutableList<Pair<String, () -> String>> = mutableListOf()

    fun filter(predicate: (ServerCall<*, *>) -> Boolean) {
      filters += predicate
    }

    fun tag(name: String, lambda: () -> String) {
      tags += name to lambda
    }
  }

  private val config: Configuration = Configuration().apply(configure)
  private val tracer: Tracer = GlobalOpenTelemetry.getTracer(this.config.instrumentation)

  override fun <ReqT : Any, RespT : Any> interceptCall(
    call: ServerCall<ReqT, RespT>,
    headers: Metadata,
    next: ServerCallHandler<ReqT, RespT>
  ): ServerCall.Listener<ReqT> {
    if (config.filters.isNotEmpty() && config.filters.all { !it(call) }) return next.startCall(call, headers)

    val parent = GlobalOpenTelemetry
      .getPropagators()
      .textMapPropagator
      .extract(
        Context.current(),
        headers,
        object : TextMapGetter<Metadata> {
          override fun keys(carrier: Metadata): MutableIterable<String> =
            carrier.keys()

          override fun get(carrier: Metadata?, key: String): String? =
            carrier?.get(Metadata.Key.of(key, Metadata.ASCII_STRING_MARSHALLER))
        }
      )

    parent.makeCurrent()

    val span = this.tracer
      .spanBuilder(call.methodDescriptor.fullMethodName)
      .setSpanKind(SpanKind.SERVER)
      .setParent(parent)
      .setAttribute(AttributeKey.stringKey("grpc.method"), call.methodDescriptor.fullMethodName)
      .setAttribute(AttributeKey.stringKey("grpc.service"), call.methodDescriptor.serviceName ?: "")
      .startSpan()

    this.config.tags.forEach {
      span.setAttribute(it.first, it.second.invoke())
    }
    span.makeCurrent()

    return object : ForwardingServerCallListener.SimpleForwardingServerCallListener<ReqT>(
      next.startCall(
        object : ForwardingServerCall.SimpleForwardingServerCall<ReqT, RespT>(call) {
          override fun close(status: Status, trailers: Metadata) {
            MDC.put("traceId", span.spanContext.traceId)
            span.setAttribute(AttributeKey.stringKey("grpc.status.name"), status.code.name)
            span.setAttribute(AttributeKey.longKey("grpc.status.value"), status.code.value())
            super.close(status, trailers)
          }
        },
        headers
      )
    ) {
      override fun onComplete() {
        span.setStatus(StatusCode.OK)
        span.end()

        super.onComplete()
      }

      override fun onCancel() {
        span.setAttribute(AttributeKey.booleanKey("error"), true)
        span.setStatus(StatusCode.ERROR)
        span.end()

        super.onCancel()
      }
    }
  }

  companion object Interceptor : Tracing()
}
