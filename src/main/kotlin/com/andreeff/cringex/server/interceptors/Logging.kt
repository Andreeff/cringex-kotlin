package com.andreeff.cringex.server.interceptors

import com.andreeff.cringex.ext.colored
import io.grpc.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.event.Level

open class Logging(configure: Configuration.() -> Unit = {}) : ServerInterceptor {
  class Configuration {
    val filters: MutableList<(ServerCall<*, *>) -> Boolean> = mutableListOf()
    var format: (ServerCall<*, *>, Status) -> String =
      { call, status -> "${status.colored(isColored)}: ${call.methodDescriptor.fullMethodName}" }
    var isColored: Boolean = true
    var level: Level = Level.INFO

    fun filter(predicate: (ServerCall<*, *>) -> Boolean) {
      filters += predicate
    }
  }

  private val config: Configuration = Configuration().apply(configure)
  private val logger: Logger = LoggerFactory.getLogger("ktor.application")

  override fun <ReqT : Any, RespT : Any> interceptCall(
    call: ServerCall<ReqT, RespT>,
    headers: Metadata,
    next: ServerCallHandler<ReqT, RespT>
  ): ServerCall.Listener<ReqT> {
    if (config.filters.isNotEmpty() && config.filters.all { !it(call) }) return next.startCall(call, headers)

    return next.startCall(
      object : ForwardingServerCall.SimpleForwardingServerCall<ReqT, RespT>(call) {
        override fun close(status: Status, trailers: Metadata) {
          super.close(status, trailers)

          when (config.level) {
            Level.TRACE -> logger.trace(config.format(call, status))
            Level.DEBUG -> logger.debug(config.format(call, status))
            Level.INFO -> logger.info(config.format(call, status))
            Level.WARN -> logger.warn(config.format(call, status))
            Level.ERROR -> logger.error(config.format(call, status))
          }
        }
      },
      headers
    )
  }

  companion object Interceptor : Logging()
}
