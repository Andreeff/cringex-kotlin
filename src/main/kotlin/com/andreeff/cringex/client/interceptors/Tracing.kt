package com.andreeff.cringex.client.interceptors

import io.grpc.*
import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.common.AttributeKey
import io.opentelemetry.api.trace.Span
import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.Context

open class Tracing(configure: Configuration.() -> Unit = {}) : ClientInterceptor {
  class Configuration {
    var instrumentation: String = "CringeX"
    val tags: MutableList<Pair<String, () -> String>> = mutableListOf()

    fun tag(name: String, lambda: () -> String) {
      tags += name to lambda
    }
  }

  private val config: Configuration = Configuration().apply(configure)
  private val tracer: Tracer = GlobalOpenTelemetry.getTracer(this.config.instrumentation)

  override fun <ReqT : Any, RespT : Any> interceptCall(
    method: MethodDescriptor<ReqT, RespT>,
    callOptions: CallOptions,
    next: Channel
  ): ClientCall<ReqT, RespT> {
    val span = this.tracer
      .spanBuilder("Call to ${method.fullMethodName}")
      .setSpanKind(SpanKind.CLIENT)
      .setAttribute(AttributeKey.stringKey("grpc.method"), method.fullMethodName)
      .setAttribute(AttributeKey.stringKey("grpc.service"), method.serviceName ?: "")
      .setParent(Context.current().with(Span.current()))
      .startSpan()

    this.config.tags.forEach {
      span.setAttribute(it.first, it.second.invoke())
    }
    span.makeCurrent()

    return object : ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(next.newCall(method, callOptions)) {
      override fun start(responseListener: Listener<RespT>, headers: Metadata) {
        GlobalOpenTelemetry
          .getPropagators()
          .textMapPropagator
          .inject(
            Context.current(),
            headers
          ) { carrier, key, value ->
            carrier?.put(Metadata.Key.of(key, Metadata.ASCII_STRING_MARSHALLER), value)
          }

        super.start(
          object : ForwardingClientCallListener.SimpleForwardingClientCallListener<RespT>(responseListener) {
            override fun onClose(status: Status, trailers: Metadata) {
              if (!status.isOk) {
                span.setAttribute(AttributeKey.booleanKey("error"), true)
                span.setStatus(StatusCode.ERROR)
              } else {
                span.setStatus(StatusCode.OK)
              }

              span.setAttribute(AttributeKey.stringKey("grpc.status.name"), status.code.name)
              span.setAttribute(AttributeKey.longKey("grpc.status.value"), status.code.value())
              span.end()

              super.onClose(status, trailers)
            }
          },
          headers
        )
      }
    }
  }

  companion object Interceptor : Tracing()
}
