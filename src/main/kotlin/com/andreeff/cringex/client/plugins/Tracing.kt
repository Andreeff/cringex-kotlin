package com.andreeff.cringex.client.plugins

import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.util.*
import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.common.AttributeKey
import io.opentelemetry.api.trace.Span
import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.Context
import kotlinx.coroutines.Job

class Tracing(private val config: Configuration) {
  class Configuration {
    var instrumentation: String = "CringeX"
    val tags: MutableList<Pair<String, () -> String>> = mutableListOf()

    fun tag(name: String, lambda: () -> String) {
      tags += name to lambda
    }
  }

  private val tracer: Tracer = GlobalOpenTelemetry.getTracer(this.config.instrumentation)

  companion object Plugin : HttpClientPlugin<Configuration, Tracing> {
    override val key: io.ktor.util.AttributeKey<Tracing> = AttributeKey("Tracing")

    override fun prepare(block: Configuration.() -> Unit): Tracing = Tracing(Configuration().apply(block))

    override fun install(plugin: Tracing, scope: HttpClient) {
      scope.sendPipeline.intercept(HttpSendPipeline.State) {
        val span = plugin.tracer
          .spanBuilder("Call to ${context.method.value} ${context.url.host}${context.url.encodedPath}")
          .setSpanKind(SpanKind.CLIENT)
          .setAttribute(AttributeKey.stringKey("http.method"), context.method.value)
          .setAttribute(AttributeKey.stringKey("http.url"), "${context.url.host}${context.url.encodedPath}")
          .setParent(Context.current().with(Span.current()))
          .startSpan()

        span.makeCurrent()
        plugin.config.tags.forEach {
          span.setAttribute(it.first, it.second.invoke())
        }
        coroutineContext[Job]?.invokeOnCompletion {
          it?.also {
            span.setAttribute(AttributeKey.booleanKey("error"), true)
            span.recordException(it)
            finish()
          }
        }
        GlobalOpenTelemetry
          .getPropagators()
          .textMapPropagator
          .inject(
            Context.current(),
            context
          ) { carrier, key, value -> carrier?.headers?.set(key, value) }
      }
      scope.receivePipeline.intercept(HttpReceivePipeline.State) {
        val span = Span.current()

        if (subject.status.value >= 400) {
          span.setAttribute(AttributeKey.booleanKey("error"), true)
          span.setStatus(StatusCode.ERROR)
        } else {
          span.setStatus(StatusCode.OK)
        }

        span.setAttribute(AttributeKey.longKey("http.status_code"), subject.status.value)

        span.end()
      }
    }
  }
}
