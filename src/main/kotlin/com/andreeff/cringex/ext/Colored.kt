package com.andreeff.cringex.ext

import io.grpc.Status
import org.fusesource.jansi.Ansi
import org.fusesource.jansi.AnsiConsole

fun Status.colored(isColored: Boolean = true): String {
  if (!isColored) {
    return code.name
  }

  try {
    if (!AnsiConsole.isInstalled()) {
      AnsiConsole.systemInstall()
    }
  } catch (_: Throwable) {
    return code.name
  }

  return when (this) {
    Status.OK -> colored(code.name, Ansi.Color.GREEN)
    else -> colored(code.name, Ansi.Color.RED)
  }
}

fun colored(value: Any, color: Ansi.Color): String =
  Ansi.ansi().fg(color).a(value).reset().toString()
